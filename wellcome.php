<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

<?php
//print_r($_POST);
$forename = $_POST['Forename'];
$surname = $_POST['Surname'];
$username = $_POST['Username'];
$password = $_POST['Password'];
$age = $_POST['Age'];
$email = $_POST['Email'];
$error = '';

if(empty($forename) || empty($surname) || empty($username) || empty($password) || empty($age) || empty($email)){
    $error .= '<br> Empty Field.';
}
if(!preg_match("/^([a-zA-Z']+){3,}$/",$forename)){
    $error .= '<br> forename - Must not be blank, must not contain spaces, and must have at least 3 alphabet characters.';
}
if(!preg_match("/^([a-zA-Z']+){3,}$/",$surname)){
    $error .= '<br> surname - Must not be blank, must not contain spaces, and must have at least 3 alphabet characters.';
    }
if(!preg_match("/^[a-zA-Z0-9_-]{5,}$/", $username)){
    $error .= '<br> username - At least 5 characters and can include numbers, _ and – .';
}
/*
Minimum eight characters, at least one letter and one number: "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
Minimum eight characters, at least one letter, one number and one special character:"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
Minimum eight characters, at least one uppercase letter, one lowercase letter and one number:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$"
Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}"
Minimum eight and maximum 10 characters, at least one uppercase letter, one lowercase letter, one number and one special character:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,10}"
 */
if(!preg_match("/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/",$password)){
    $error .= '<br> password - Must be at least 8 characters, containing both upper and lower case letters, numbers and symbols.';
}

if(!($age >= 18) || !($age <= 110)){
    $error .= '<br> age - It is a 18+ website, so the age must be between 18 and 110.';
}
if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i",$email)){
    $error .= '<br> email - must be of the form abc@def.ghi.';
}
if(!empty($error)){
    $output_error = '<div class="alert alert-danger"><strong>Danger!</strong> ' . $error . '</div>';
    echo $output_error;
}
//Assignment Ken's baby store will come later before Mid-term. :D
?>