<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Index</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
</head>
<body>
    <div class="container">
    <div class="row align-items-center justify-content-center">
    <form action="wellcome.php" method="POST">
        <div class="form-group">
        <label for="forename">Forename</label>
        <input type="text" name="Forename" class="form-control" id="forename">
        <small id="forenameHelp" class="form-text text-muted">Must not be blank, must not contain spaces, and must have at least 3 alphabet characters.</small>
        </div>
        <div class="form-group">
        <label for="surname">Surname</label>
        <input type="text" name="Surname" class="form-control" id="surname">
        <small id="surnameHelp" class="form-text text-muted">Must not be blank, must not contain spaces, and must have at least 3 alphabet characters.</small>
        </div>
        <div class="form-group">
        <label for="username">Username</label>
        <input type="text" name="Username" class="form-control" id="username">
        <small id="usernameHelp" class="form-text text-muted">At least 5 characters and can include numbers, _ and – .</small>
        </div>
        <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="Password" class="form-control" id="password">
        <small id="passwordHelp" class="form-text text-muted">Must be at least 8 characters, containing both upper and lower case letters, numbers and symbols.</small>
        </div>
        <div class="form-group">
        <label for="age">Age</label>
        <input type="number" min="18" max="110" name="Age" class="form-control" id="age">
        <small id="ageHelp" class="form-text text-muted">It is a 18+ website, so the age must be between 18 and 110.</small>
        </div>
        <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="Email" class="form-control" id="email">
        <small id="emailHelp" class="form-text text-muted">must be of the form abc@def.ghi.</small>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
    </div>
</body>
</html>